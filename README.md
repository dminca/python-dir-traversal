# UNIX directory operations
> Some basic operations on UNIX directories such as list directories, calculate
total size of certain directories that match a pattern et. al.

## Requirements
> The requirements listed below refer to Python libraries, some are present
natively.
- `glob`
- `os`

### Useful resources
- [Get filtered list of files in a directory][1]
- [Calculate directory sizes][2]

[1]: https://docs.python.org/3/library/glob.html#glob.glob
[2]: https://stackoverflow.com/a/12480543/2733115