#!/usr/bin/env python3
# Calculate directory size
import os
import re
from os.path import getsize, join

DIR_PATH = "foobar/src/"


class DirectoryTraversal:
    """Calculate filtered directory sizes"""

    def __init__(self, pattern):
        self.pattern = pattern
        self.calculate_directory_size()

    def calculate_directory_size(self):
        """
        Calculate directory sizes based on a regex pattern.
        :return: directory size in bytes
        """
        dirs_dict = dict()
        dir_size_list = list()
        for root, dirs, files in os.walk(DIR_PATH, topdown=False):
            # loop through every non directory file in this dir and sum their sizes
            if re.search(self.pattern, root):
                size = sum(getsize(join(root, name)) for name in files)
                my_size = dirs_dict[root] = size
                dir_size_list.append(my_size)
        print("{} {}".format(sum(dir_size_list), "bytes"))


def main():
    pattern = r"2018-0[1-3]"
    DirectoryTraversal(pattern)


if __name__ == '__main__':
    main()
